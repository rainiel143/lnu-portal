<?php

namespace App\Http\Controllers;
use App\Models\Students;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Students::all();

        return response()->json($students);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $students = new Students;

        $students->username = $request->input('username');
        $students->password = $request->input('password');
        $students->id_number = $request->input('id_number');
        $students->first_name = $request->input('first_name');
        $students->last_name = $request->input('last_name');
        $students->middle_name = $request->input('middle_name');
        $students->gender = $request->input('gender');
        $students->year = $request->input('year');
        $students->section = $request->input('section');
        $students->mobile_number = $request->input('mobile_number');
        $students->email = $request->input('email');
        $students->address = $request->input('address');

        $students->save();

        return response()->json($students);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $students = Students::find($id);

        $students->username = $request->input('username');
        $students->password = $request->input('password');
        $students->id_number = $request->input('id_number');
        $students->first_name = $request->input('first_name');
        $students->last_name = $request->input('last_name');
        $students->middle_name = $request->input('middle_name');
        $students->gender = $request->input('gender');
        $students->year = $request->input('year');
        $students->section = $request->input('section');
        $students->mobile_number = $request->input('mobile_number');
        $students->email = $request->input('email');
        $students->address = $request->input('address');

        $students->save();

        return response()->json($students);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $students = Students::find($id);

        $students->delete();

        return response()->json($students);
    }
}
