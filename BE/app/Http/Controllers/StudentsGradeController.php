<?php

namespace App\Http\Controllers;
use App\Models\StudentsGrade;
use Illuminate\Http\Request;

class StudentsGradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students_pending = StudentsGrade::where('grades_status', 'INC')->get();
        $students_complete = StudentsGrade::where('grades_status', '>=', 1)->get();

        return response()->json(['pending' => $students_pending, 'complete' =>  $students_complete ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $students = new StudentsGrade;

        $students->students_id = $request->input('students_id');
        $students->grades_status = $request->input('grades_status');

        $students->save();

        return response()->json($students);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $students = StudentsGrade::find($id);

        $students->students_id = $request->input('students_id');
        $students->grades_status = $request->input('grades_status');

        $students->save();

        return response()->json($students);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $students = StudentsGrade::find($id);

        $students->delete();

        return response()->json($students);
    }
}
