<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Students extends Model
{
    use HasFactory;

    protected $table = 'students';
    protected $fillable = ['username','password','id_number','first_name','middle_name','last_name','gender','year','section','mobile_number','email','address'];

    public function students_grade()
    {
        return $this->hasMany(StudentsGrade::class);
    }
}
