<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentsGrade extends Model
{
    use HasFactory;

    protected $table = 'students_grade';
    protected $fillable = ['students_id','grades_status'];

    public function students()
    {
        return $this->belongsTo(Students::class);
    }
}
