import Vue from 'vue'
import VueRouter from "vue-router";
import Home from '../components/Student/Home.vue'
import Students from '../components/Students.vue'
import Login from '../components/Login.vue'
import Dashboard from '../components/Dashboard.vue'

Vue.use(VueRouter);

export default new VueRouter({
    routes:  [
      {
        path: "/",
        component: Login
      },
      {
        path: "/dashboard",
        component: Dashboard
      },
      {
        path: "/students",
        component: Students
      },
      {
        path: "/login",
        component: Login
      },
      {
        path: "/home",
        component: Home
      },
    ]
  }); 