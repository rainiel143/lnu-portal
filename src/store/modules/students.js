import axios from "axios";

export default {
    state: () => ({
        studentsData: []
    }),
    mutations: {
        STUDENTS_DATA(state,data){
            state.studentsData = data

        }
    },
    actions:{
        getData({commit}){
            axios.get('http://127.0.0.1:8001/api/students')
            .then(response => { 
                commit('STUDENTS_DATA', response.data)
            })
            .catch(error => {
                console.log(error)
            })
        },
        postData({actions}){
            axios.post

        }
    },
    getters:{ } 
}